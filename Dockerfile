# The following instruction is equal to the line below, but uses gitlabs Dependency Proxy
# FROM python:3.11-alpine
FROM gitlab.com:443/nest.lbl.gov/dependency_proxy/containers/python:3.11-alpine

LABEL maintainer="Simon Patton <sjpatton@lbl.gov, Keith Beattie <ksbeattie@lbl.gov>"

ENV PYTHONUNBUFFERED=1 \
    PIP_ROOT_USER_ACTION=ignore

COPY requirements.txt requirements.txt

# build-base, linux-headers and python3-dev is only here because there is not (yet) a wheel on pypi
# for psutils for python 3.11
RUN apk update \
    && apk add --no-cache bash openssh-client procps build-base linux-headers python3-dev \
    && pip install -r requirements.txt --upgrade pip \
    && apk del build-base linux-headers python3-dev

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

COPY bin /root/bin