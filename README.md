# What is lblnest/ssh-tunnel? #

The `lblnest/ssh-tunnel` image is designed to be used in conjunction with a collection of other images that are group of services within an orchestration unit. This unit may be a [kubernetes namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/), [Docker swarm](https://docs.docker.com/engine/swarm/), or similar orchestration systems. Within this orchestration unit the `lblnest/ssh-tunnel` image enables access to services not directly accessible from outside their deployed environment as well as providing those services with access to external firewalled services.

Access to and by the services within an orchestration unit is done by [`ssh` tunneling](https://www.ssh.com/academy/ssh/tunneling), also know as port forwarding. When the image is run, it reads the provided [`ssh_config` file](https://www.ssh.com/academy/ssh/config) and establishes the necessary tunnels to allow traffic in and out of the unit.


## The `TUNNEL_SSH_CONFIG` environment variable ##

The `ssh_config` file used by this image is specified by the container's `TUNNEL_SSH_CONFIG` environment variable. Normally this is set to point to a "secret" so that the image's actions are full customizable yet it can be deployed unchanged.


## The `ssh_config` file ##

While the `ssh_config` file can be anything you want, here is some guidance on how to put one together.


### `StrictHostKeyChecking` ###

On of the first issue you should decide is whether you will be using the `StrictHostKeyChecking` option. When this option is "yes" (the default), `ssh` will only connection to hosts whose host-keys are in the `UserKnownHostsFile` file, the default location of this being `${HOME}/.ssh/known_hosts`. In order to use the default (and recommended) setting for the `StrictHostKeyChecking` option, it is good to make the first entry of the `ssh_config` file a mapping of the `UserKnownHostsFile` file to a secret. Thus the first part of the `ssh_config` file should read something like this (where `/etc/run/secrets/` is the mount point of the container's secrets):

    UserKnownHostsFile /etc/run/secrets/known_hosts

The contents of the `known_hosts` secret can be obtained using the `ssh-keyscan` command. For example the following command will return all of the key types for [gitlab.com](https://gitlab.com).

    ssh-keyscan gitlab.com

If you want to limit the type of keys returned, you can specify the type of key you want in the command. For example, the following is the recommended one at the moment as the `ssh` client in the image supports "ed25519".

    ssh-keyscan -t ssh-ed25519 gitlab.com

If, for some reason, you can not get hold of the host-key of any of the target hosts, or you do not want host-key checking for some (bizarre?) reason, then you will want the following to be the first part of your `ssh_config` file.

    StrictHostKeyChecking No


### Remote User Name and Key ###

The next step in setting up this image is the creation of a user account on the remote host in order for `ssh` to be able to establish a session there. Exactly how this is done depends on the remote host and will not be discussed here, suffice to say that at the end of the process you want to have created the following on the remote host. (The remote host's name will replace the `<remote_host>` placeholder in the rest of this document.)

* A User account that will be used just for this tunnelling. This will replace the `<remote_user>` placeholder in the rest of this document.

* A public/private key pair with *no* passphrase. The passphrase is left empty as the image can not prompt for a value. The private key name will replace the `<user_key>` placeholder in the rest of this document, while the public key contents should be copied in the `.ssh/authorized_keys` file for the user on the remote host. (While doing this, make sure all of the files and directories have the correct permissions as this is one of the primary reasons `ssh` will fail.)


### Declaring the connection ###

With the remote user name and key established, the authentication of the `ssh` connection *from* the image *to* the remote host can be declared in the `ssh_config` file. This section of the file should look like this (again where `/etc/run/secrets/` is the mount point of the container's secrets):

    Host tnl_<label>
        Hostname <remote_host>
        User <remote_user>
        IdentityFile /etc/run/secrets/<user_key>

In this section `<label>` is some short string to identify this tunnel form any others that may be declared.

The contents of the `<user_key>` secret is the *private* key generated on the remote host.


### Tunnel Directions ###

`ssh` tunnels come in two flavors, "local" and "remote". The difference between these is where the exposed end of the tunnel will be. A "local" tunnel is used to map a remote port onto a port on the local machine. For example, if there is a database listening on a port on a machine behind a firewall, and the remote host is also behind this firewall, using the [LocalForwarding](https://www.ssh.com/academy/ssh/tunneling/example#local-forwarding) option enables you to access that database using a port on the "localhost" machine.

Conversely, if you are behind the firewall, along with the database, using the [RemoteForwarding](https://www.ssh.com/academy/ssh/tunneling/example#remote-forwarding) option enables you to access that database a port on the "remote" machine.


#### Local Tunnels ####

To set up a local tunnel using this image, i.e. provide services within the orchestration unit with access to a firewalled port, the following lines should be added to the `Host` block in the `ssh_config` file.

        LocalForward <container_port> <target_host>:<target_port>
        GatewayPorts yes

In this case the `<container_port>` is the port this image's container through which the tunnel can be accessed. The `<target_host>` is the host, visible to the remote host, one which the tunnelled port exists and the `<target_port>` is the port on that target host.

The `GatewayPorts yes` option means that the container's port will listen on "0.0.0.0" so that all of the other containers with the orchestration unit can connect to it. (Without the `GatewayPorts` option the port will on listen on the "localhost" interface and not be available to the other containers.)


#### Remote Tunnels ####

To set up a remote tunnel using this image, i.e. provide access to services within the orchestration unit that is firewalled, the following lines should be added to the `Host` block in the `ssh_config` file.

        RemoteForward <remote_port> <source_container>:<source_port>

In this case the `<remote_port>` is the port on the remote host through which the tunnel will be accessed. The `<source_container>` is the container within the orchestration unit whose service is being exposed and the `<source_port>` is the port on that container on which that service is listening.

In the case of remote tunnels, if the tunnel is to be accessed by any other host as well as the remote host itself, the `GatewayPorts` option needs to be set in the `sshd_config` file on the remote host, and *not** in the `ssh_config` file for this image. On the remote host, the recommended approach is to add the following lines to the bottom of the `sshd_config` file and restart the `sshd` service.

    Match User <remote_user>
        GatewayPorts yes

This limits the exposure of tunnelled ports to only those created by the specified user.


## Example ##

Here is an example `ssh_config` file that will create two tunnels, one local and one remote.


    UserKnownHostsFile /etc/run/secrets/known_hosts

    Host tnl_<label>
        Hostname <remote_host>
        User <remote_user>
        IdentityFile /etc/run/secrets/<user_key>
        LocalForward <container_port> <target_host>:<target_port>
        GatewayPorts yes

    Host tnl_<label2>
        Hostname <remote_host2>
        User <remote_user2>
        IdentityFile /etc/run/secrets/<user_key2>
        RemoteForward <remote_port> <source_container>:<source_port>

The `tnl_<label>` tunnel provides access to a firewalled service for all services with the orchestration unit. This access is via the ``<container>:<container_port>`` address.

The `tnl_<label2>` tunnel is used to expose the service listening on `<source_container>`:`<source_port>` so that it can be accessed via the `<remote_host2>:<remote_port>` address.
